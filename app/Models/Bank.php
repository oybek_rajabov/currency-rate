<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use SDamian\Larasort\AutoSortable;

class Bank extends Model
{
    use HasFactory, AutoSortable;
    protected $table = 'banks';
    protected $guarded = [];
    private array $sortables = [ // The attributes that are sortable.
        'name',
    ];

}
