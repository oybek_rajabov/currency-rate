<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory, HasTranslations;
    public $translatable = ['footer_text'];
    protected $table = 'about';
    protected $guarded = [];


    public function getFooterTextAttribute($value)
    {
        return $this->translate('footer_text') ?? ($value ?? '');
    }
}
