<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use SDamian\Larasort\AutoSortable;

class Seo extends Model
{
    use HasFactory, HasTranslations;
    public $translatable = ['title', 'description', 'keywords'];
    protected $table = 'seo';
    protected $guarded = [];

    public function getTitleAttribute($value)
    {
        return $this->translate('title') ?? ($value ?? '');
    }

    public function getDescriptionAttribute($value)
    {
        return $this->translate('description') ?? ($value ?? '');
    }

    public function getKeywordsAttribute($value)
    {
        return $this->translate('keywords') ?? ($value ?? '');
    }
}
