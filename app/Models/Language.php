<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use SDamian\Larasort\AutoSortable;

class Language extends Model
{
    use HasFactory, AutoSortable;
    protected $table = 'languages';
    protected $guarded = [];
}
