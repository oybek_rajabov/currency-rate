<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use SDamian\Larasort\AutoSortable;

class Currency extends Model
{
    use HasFactory, AutoSortable, HasTranslations;
    public $translatable = ['cur_name','seo_title','seo_description','seo_keywords'];
    protected $table = 'currencies';
    protected $guarded = [];
    private array $sortables = [ // The attributes that are sortable.
        'base_code',
        'name',
    ];

    public function getCurNameAttribute($value)
    {
        return $this->translate('cur_name') ?? ($value ?? '');
    }
}
