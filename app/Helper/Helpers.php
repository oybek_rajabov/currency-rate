<?php

use App\Models\About;
use App\Models\Language;

function moneyFormat($value){
    $result = '';
    if($value < 1000)
    {
        return $value;
    }

    $reminder = intval(fmod($value, 1)*100);
    while ($value >= 1000){
        $n = intval($value/1000);
             if($n < 10) $result = $n.' '.$result;
             if($n > 10 && $n < 100) $result = $n.' '.$result;
             else $result = $n.' '.$result;
        $value = $value % 1000;
        if($value < 1000){
            if($value < 10) return $result.strval($value).'.'.strval($reminder);
            elseif($value > 10 && $value < 100) return $result.strval($value).'.'.strval($reminder);
            elseif ($value >= 100) return $result.strval($value).'.'.strval($reminder);
        }
    }
    return $result;
}

function randomClasses(){
    $classes = [
        'bg-aqua',
        'bg-black',
        'bg-blue',
        'bg-green',
        'bg-orange',
        'bg-primary',
        'bg-purple',
    ];
    shuffle($classes);
    return $classes;
}

function defaultLocale()
{
    return Language::where('default', true)->first();
}

function allLanguage()
{
    return Language::all();
}

function defaultLocaleCode()
{
    return optional(defaultLocale())->url;
}

function seoTypes(){
    return [
          'main_page' => 'Главная страница',
        'currency_detail' => 'Страница расчета валюты'
    ];
}

function footerText(){
    $about = About::query()->first();
    return $about->footer_text;
}
