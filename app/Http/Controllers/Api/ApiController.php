<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Currency;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{

    public function importCurrencyDatas()
    {
        $currenciesDatas = Http::get('https://v6.exchangerate-api.com/v6/'.env('API_KEY').'/codes')->collect();
        $currencies = $currenciesDatas['supported_codes'];
        foreach ($currencies as $currency){
            Currency::updateOrCreate([
                'base_code' => $currency[0],
            ],[
                'base_code' => $currency[0],
                'cur_name' => $currency[1]
            ]);
        }
        return true;
    }

    public function updateRate(Request $request)
    {
        $currencies = Currency::all();
        ini_set('max_execution_time', 240);
        foreach ($currencies as $currency) {
            if($currency->rate_update_at == null)
            {
                $response = Http::get('https://v6.exchangerate-api.com/v6/'.env('API_KEY').'/latest/'.$currency->base_code)->collect();
                if($response['result'] == 'success')
                {
                    $currency->update([
                        'conversion_rates' => json_encode($response['conversion_rates']),
                        'rate_update_at' => date('Y-m-d H:i:s')
                    ]);
                    continue;
                } else continue;
            }
            $diff = (strtotime(date('Y-m-d H:i:s')) - strtotime($currency->rate_update_at))/3600;
            if($diff > 12){
                $response = Http::get('https://v6.exchangerate-api.com/v6/'.env('API_KEY').'/latest/'.$currency->base_code)->collect();
                if($response['result'] == 'success')
                {
                    $currency->update([
                        'conversion_rates' => json_encode($response['conversion_rates']),
                        'rate_update_at' => date('Y-m-d H:i:s')
                    ]);
                    continue;
                }else continue;
            }
        }
        return true;
    }

    public function scraper()
    {
        $client = new Client();
        $page = $client->request('GET', env('BANKS_RATE_URL'));
        $page->filter('table#droptablesTbl45 tbody tr')->each(function ($item) {
            $this->banks[] = [
                'name' => $item->filter('td')->getNode(0)->textContent,
                'usd_buy' => $item->filter('td')->getNode(1)->textContent,
                'usd_sell' => $item->filter('td')->getNode(2)->textContent,
                'rub_buy' => $item->filter('td')->getNode(3)->textContent,
                'rub_sell' => $item->filter('td')->getNode(4)->textContent,
                'eur_buy' => $item->filter('td')->getNode(5)->textContent,
                'eur_sell' => $item->filter('td')->getNode(6)->textContent,
                'kzt_buy' => $item->filter('td')->getNode(7)->textContent,
                'kzt_sell' => $item->filter('td')->getNode(8)->textContent,
            ];
        });
        foreach ($this->banks as $bank){
            Bank::updateOrCreate([
                'name' => $bank['name']
            ],[
                'name' => $bank['name'],
                'usd_buy' => floatval($bank['usd_buy']),
                'usd_sell' => floatval($bank['usd_sell']),
                'rub_buy' => floatval($bank['rub_buy']),
                'rub_sell' => floatval($bank['rub_sell']),
                'eur_buy' => floatval($bank['eur_buy']),
                'eur_sell' => floatval($bank['eur_sell']),
                'kzt_buy' => floatval($bank['kzt_buy']),
                'kzt_sell' => floatval($bank['kzt_sell']),
            ]);
        }
        return true;
    }

}
