<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class LocalizationController extends Controller
{
    public function setLang($locale)
    {
        App::setLocale($locale);
        Session::put('locale', $locale);
        if($locale == 'ru')
            return redirect()->route('currencies');
        else
        return redirect()->route('currencies',['locale' => $locale]);
    }
}
