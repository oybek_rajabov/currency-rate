<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Seo;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $about = About::query()->first();
        $translates = [
            'footer_text' => $about->pluckTranslates('footer_text'),
        ];
        return view('admin.about.index', compact('about','translates'));
    }

    public function edit(About $id)
    {
        $about = $id;
        $translates = [
            'footer_text' => $about->pluckTranslates('footer_text'),
        ];
        return view('admin.about.update', compact('about', 'translates'));
    }

    public function update(Request $request, About $about)
    {
        $request->validate([
            'footer_text_ru' => ['required', 'string'],
            'footer_text_en' => ['required', 'string'],
            'footer_text_uz' => ['required', 'string']
        ]);
        $about->update([
            'footer_text' => $request->footer_text_ru,
        ]);
        $translates = [
            'footer_text' => ['ru' => $request->footer_text_ru ?? '', 'uz' => $request->footer_text_uz ?? '', 'en' => $request->footer_text_en],
        ];
        $about->setTranslationsArray($translates);
        return redirect()->route('admin.about.index');
    }

}
