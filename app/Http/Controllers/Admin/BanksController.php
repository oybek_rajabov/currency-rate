<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Goutte\Client;
use Illuminate\Http\Request;

class BanksController extends Controller
{
    private $banks = [];
    public function index()
    {
        $banks = Bank::query()->autosort()->get();
        return view('admin.banks.index', compact('banks'));
    }

    public function scraper()
    {
        $client = new Client();
        $page = $client->request('GET', env('BANKS_RATE_URL'));
        $page->filter('table#droptablesTbl45 tbody tr')->each(function ($item) {
            $this->banks[] = [
                'name' => str_replace('вЂ™',"'",$item->filter('td')->getNode(0)->textContent),
                'usd_buy' => $item->filter('td')->getNode(1)->textContent,
                'usd_sell' => $item->filter('td')->getNode(2)->textContent,
                'rub_buy' => $item->filter('td')->getNode(3)->textContent,
                'rub_sell' => $item->filter('td')->getNode(4)->textContent,
                'eur_buy' => $item->filter('td')->getNode(5)->textContent,
                'eur_sell' => $item->filter('td')->getNode(6)->textContent,
                'kzt_buy' => $item->filter('td')->getNode(7)->textContent,
                'kzt_sell' => $item->filter('td')->getNode(8)->textContent,
            ];
        });
        foreach ($this->banks as $bank){
           Bank::updateOrCreate([
               'name' => $bank['name']
           ],[
               'name' => $bank['name'],
               'usd_buy' => floatval($bank['usd_buy']),
               'usd_sell' => floatval($bank['usd_sell']),
               'rub_buy' => floatval($bank['rub_buy']),
               'rub_sell' => floatval($bank['rub_sell']),
               'eur_buy' => floatval($bank['eur_buy']),
               'eur_sell' => floatval($bank['eur_sell']),
               'kzt_buy' => floatval($bank['kzt_buy']),
               'kzt_sell' => floatval($bank['kzt_sell']),
           ]);
        }
        return back();
    }

    public function show($id)
    {
        $bank = Bank::query()->where('id',$id)->first();
        return view('admin.banks.show', compact('bank'));
    }

    public function edit(Bank $id)
    {
        $bank = $id;
        return view('admin.banks.update',compact('bank',));
    }

    public function update(Request $request, Bank $bank)
    {
        $request->validate([
            'name' => ['required', 'string']
        ]);
        $datas = $request->all();
        unset($datas['_method']);
        unset($datas['_token']);
        $bank->update($datas);
        return redirect()->route('admin.banks.index');
    }

    public function delete(Bank $bank)
    {
        if($bank->delete())
            return 1;
        else return 0;
    }
}
