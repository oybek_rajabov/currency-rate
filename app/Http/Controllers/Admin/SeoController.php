<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Seo;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    public function index()
    {
        $seos = Seo::all();
        return view('admin.seo.index', compact('seos'));
    }

    public function create()
    {
        return view('admin.seo.create');
    }

    public function insert(Request $request)
    {
        $request->validate([
            'type' => ['required'],
            'seo_title_ru' => ['required'],
            'seo_description_ru' => ['required'],
            'seo_keywords_ru' => ['required']
        ]);
        $seo = Seo::create([
            'type' => $request->type,
            'title' => $request->seo_title_ru,
            'description' => $request->seo_description_ru,
            'keywords' => $request->seo_keywords_ru,
        ]);
        $translates = [
            'title' => ['ru' => $request->seo_title_ru ?? '', 'uz' => $request->seo_title_uz ?? '', 'en' => $request->seo_title_en],
            'description' => ['ru' => $request->seo_description_ru ?? '', 'uz' => $request->seo_description_uz ?? '', 'en' => $request->seo_description_en],
            'keywords' => ['ru' => $request->seo_keywords_ru ?? '', 'uz' => $request->seo_keywords_uz ?? '', 'en' => $request->seo_keywords_en],
        ];
        $seo->setTranslationsArray($translates);
        return redirect()->route('admin.seo.index');
    }

    public function show($id)
    {
        $seo = Seo::query()->where('id', $id)->first();
        $translate = [
            'title' => $seo->pluckTranslates('title'),
            'description' => $seo->pluckTranslates('description'),
            'keywords' => $seo->pluckTranslates('keywords'),
        ];
        return view('admin.seo.show', compact('seo', 'translate'));
    }

    public function edit(Seo $id)
    {
        $seo = $id;
        $translates = [
            'title' => $seo->pluckTranslates('title'),
            'description' => $seo->pluckTranslates('description'),
            'keywords' => $seo->pluckTranslates('keywords'),
        ];
        return view('admin.seo.update', compact('seo', 'translates'));
    }

    public function update(Request $request, Seo $seo)
    {
        $request->validate([
            'title_ru' => ['required', 'string'],
            'description_ru' => ['required', 'string'],
            'keywords_ru' => ['required', 'string']
        ]);
        $seo->update([
            'type' => $request->type,
            'title' => $request->title_ru,
            'description' => $request->description_ru,
            'keywords' => $request->keywords_ru
        ]);
        $translates = [
            'title' => ['ru' => $request->title_ru ?? '', 'uz' => $request->title_uz ?? '', 'en' => $request->title_en],
            'description' => ['ru' => $request->description_ru ?? '', 'uz' => $request->description_uz ?? '', 'en' => $request->description_en],
            'keywords' => ['ru' => $request->keywords_ru ?? '', 'uz' => $request->keywords_uz ?? '', 'en' => $request->keywords_en],
        ];
        $seo->setTranslationsArray($translates);
        return redirect()->route('admin.seo.index');
    }

    public function delete(Seo $seo)
    {
        $seo->deleteTranslations();
        if ($seo->delete())
            return 1;
        else return 0;
    }
}
