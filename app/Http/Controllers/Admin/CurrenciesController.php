<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CurrenciesController extends Controller
{
    public function index()
    {

           $currencies = Currency::query()->orderBy('is_main','asc')->autosort()->paginate(10);
           return view('admin.currencies.index', compact('currencies'));
    }

    public function show($id)
    {
        $currency = Currency::query()->where('id',$id)->first();
        $rates = json_decode($currency->conversion_rates) ?? [];
        return view('admin.currencies.show', compact('currency','rates'));
    }

    public function edit(Currency $id)
    {
        $currency = $id;
        $translates = [
            'cur_name' => $currency->pluckTranslates('cur_name'),
            'seo_title' => $currency->pluckTranslates('seo_title'),
            'seo_description' => $currency->pluckTranslates('seo_description'),
            'seo_keywords' => $currency->pluckTranslates('seo_keywords'),
        ];
        return view('admin.currencies.update',compact('currency','translates'));
    }

    public function update(Request $request, Currency $currency)
    {
        $request->validate([
            'cur_name_ru' => ['required', 'string'],
            'base_code' => ['required', 'string'],
            'country' => ['required', 'string']
        ]);
        $currency->update(['base_code' => $request->base_code, 'country' => $request->country]);
        $translates = [
            'cur_name' => ['ru' => $request->cur_name_ru ?? '', 'uz' => $request->cur_name_uz ??'', 'en' => $request->cur_name_en],
            'seo_title' => ['ru' => $request->seo_title_ru ?? '', 'uz' => $request->seo_title_uz ??'', 'en' => $request->seo_title_en],
            'seo_description' => ['ru' => $request->seo_description_ru ?? '', 'uz' => $request->seo_description_uz ??'', 'en' => $request->seo_description_en],
            'seo_keywords' => ['ru' => $request->seo_keywords_ru ?? '', 'uz' => $request->seo_keywords_uz ??'', 'en' => $request->seo_keywords_en],
        ];
        $currency->setTranslationsArray($translates);
        return redirect()->route('admin.currencies.index');
    }

    public function delete(Currency $currency)
    {
        $currency->deleteTranslations();
        if($currency->delete())
            return 1;
        else return 0;
    }

    public function importCurrencyDatas()
    {
        $currenciesDatas = Http::get('https://v6.exchangerate-api.com/v6/'.env('API_KEY').'/codes')->collect();
        $currencies = $currenciesDatas['supported_codes'];
        foreach ($currencies as $currency){
            Currency::updateOrCreate([
                'base_code' => $currency[0],
            ],[
                'base_code' => $currency[0],
                'cur_name' => $currency[1]
            ]);
        }
        return back();
    }

    public function updateRate(Request $request)
    {
        $currencies = Currency::all();
        ini_set('max_execution_time', 240);
        foreach ($currencies as $currency) {
            if($currency->rate_update_at == null)
            {
                $response = Http::get('https://v6.exchangerate-api.com/v6/'.env('API_KEY').'/latest/'.$currency->base_code)->collect();
                if($response['result'] == 'success')
                {
                    $currency->update([
                        'conversion_rates' => json_encode($response['conversion_rates']),
                        'rate_update_at' => date('Y-m-d H:i:s')
                    ]);
                    continue;
                } else continue;
            }
            $diff = (strtotime(date('Y-m-d H:i:s')) - strtotime($currency->rate_update_at))/3600;
            if($diff > 12){
                $response = Http::get('https://v6.exchangerate-api.com/v6/'.env('API_KEY').'/latest/'.$currency->base_code)->collect();
                if($response['result'] == 'success')
                {
                    $currency->update([
                        'conversion_rates' => json_encode($response['conversion_rates']),
                        'rate_update_at' => date('Y-m-d H:i:s')
                    ]);
                    continue;
                }else continue;
            }
        }
        return back();
    }

    public function changeMainCurrency(Currency $currency)
    {
        if($currency->is_main)
            $currency->update(['is_main' => false]);
        else $currency->update(['is_main' => true]);
        return 5;
    }
}
