<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Currency;
use App\Models\Seo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Result;

class CurrencyController extends Controller
{
    public function index(Request $request)
    {
        $backgroundClasses = randomClasses();
        $banks = Bank::query()->autosort()->get();
        $seo = Seo::query()->where('type','main_page')->first();
        $title = $seo->title ?? '';
        $description = $seo->description ?? '';
        $keywords= $seo->keywords ?? '';
        $mainCurrincies = Currency::where('is_main', true)->get();
        $converts = Currency::where('is_main', true)->OrWhere('base_code', 'UZS')->get();
        $allCurrencies = Currency::query()->autosort()->get();
        $all_conversion_rates = [];
        foreach ($allCurrencies as $currency) {
            $conversion_rates = json_decode($currency->conversion_rates);
            if ($conversion_rates)
                $all_conversion_rates[] = ['base_code' => $currency->base_code, 'uzs_value' => $conversion_rates->UZS];
            else
                $all_conversion_rates[] = ['base_code' => $currency->base_code, 'uzs_value' => '#'];
        }
        $datas = [];
        $i = 0;
        foreach ($mainCurrincies as $currency) {
            $uzs = $this->convertCurrency(1, $currency);
            $datas[] = [
                'id' => $currency->id,
                'name' => $currency->cur_name,
                'base_code' => $currency->base_code,
                'uzs' => $uzs . " " . __('message.soum'),
                'background' => $backgroundClasses[$i]
            ];
            $i = $i + 1;
        }
        return view('currency.index', compact('datas', 'converts', 'banks', 'all_conversion_rates','title','description','keywords'));
    }


    public function convert(Request $request)
    {
        $from = Currency::where('base_code', $request->currency1)->first();
        $rates = json_decode($from->conversion_rates);
        if ($rates)
            return sprintf('%f', $request->value * $rates->{$request->currency2});
        else return sprintf('%f', 0);
    }

    public function convertCurrency($value = 1, $from, $to = 'UZS')
    {
        $rates = json_decode($from->conversion_rates);
        if ($rates)
            return moneyFormat($value * $rates->$to);
        else return 0;
    }

    public function more($locale, Currency $currency)
    {
        $seo = Seo::query()->where('type','currency_detail')->first();
        $title = $seo->title ?? '';
        $description = $seo->description ?? '';
        $keywords= $seo->keywords ?? '';
        $converts = Currency::where('is_main', true)->OrWhere('base_code', 'UZS')->get();
        $calculator_table = [];
        for ($i = 50; $i <= 2000; $i = $i * 2) {
            $calculator_table [] = [
                'value' => $i,
                'convert' => $this->convertCurrency($i, $currency)
            ];
        }
        $uzs_convert = $this->convertCurrency(1,$currency);

        return view('currency.detail', compact('converts', 'currency', 'calculator_table','uzs_convert','keywords','description','title'));
    }
}
