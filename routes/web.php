<?php

use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\BanksController;
use App\Http\Controllers\Admin\CurrenciesController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\SeoController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\LocalizationController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|                                       
*/

Route::group(['prefix' => '', 'middleware' => 'locale'], function() {
    Route::get('{locale?}', [CurrencyController::class,'index'])->name('currencies');
    Route::get('{locale?}/currencies/more/{currency}',[CurrencyController::class,'more'])->name('currencies.more');
    Route::get('/locale/{locale}',[LocalizationController::class,'setLang'])->name('locale.set-lang');
    Route::post('/convert',[CurrencyController::class,'convert'])->name('convert');
});



///////////////////////////////////////Admin////////////////////////////////////////////////////////////
Route::get('admin/login',[AuthController::class,'login'])->name('admin.login');
Route::post('admin/check', [AuthController::class, 'check'])->name('admin.check');
Route::get('admin/logout',[AuthController::class,'logout'])->name('admin.logout');
Route::group(['prefix' => 'admin/admin','middleware' => ['auth']], function () {
    Route::get('',[HomeController::class,'index'])->name('admin.index');
    ///////////////////Currencies/////////////////////////////////////////////////
    Route::controller(CurrenciesController::class)->prefix('currencies')->group(function () {
        Route::get('', 'index')->name('admin.currencies.index');
        Route::get('/{id}', 'show')->name('admin.currencies.show');
        Route::get('/edit/{id}', 'edit')->name('admin.currencies.edit');
        Route::post('{currency}/update', 'update')->name('admin.currencies.update');
        Route::delete('/{currency}', 'delete')->name('admin.currencies.delete');
        Route::get('import/currency/datas', [CurrenciesController::class,'importCurrencyDatas'])->name('admin.currencies.import-currency-datas');
        Route::get('update/rate', [CurrenciesController::class,'updateRate'])->name('admin.currencies.update-rate');
        Route::post('/change-main-currency/{currency}', [CurrenciesController::class,'changeMainCurrency']);
    });
    //...............................................................................
    ///////////////////////////////////////Scraper//////////////////////////////////////////
    Route::controller(BanksController::class)->prefix('banks')->group(function () {
        Route::get('', 'index')->name('admin.banks.index');
        Route::get('scraper', 'scraper')->name('admin.banks.scraper');
        Route::get('/{id}', 'show')->name('admin.banks.show');
        Route::get('/edit/{id}', 'edit')->name('admin.banks.edit');
        Route::post('{bank}/update', 'update')->name('admin.banks.update');
        Route::delete('/{bank}', 'delete')->name('admin.banks.delete');
    });
    //---------------------------------------------------------------------------------------
    Route::controller(SeoController::class)->prefix('seo')->group(function () {
        Route::get('', 'index')->name('admin.seo.index');
        Route::get('/create', 'create')->name('admin.seo.create');
        Route::post('/insert', 'insert')->name('admin.seo.insert');
        Route::get('/{id}', 'show')->name('admin.seo.show');
        Route::get('/edit/{id}', 'edit')->name('admin.seo.edit');
        Route::post('{seo}/update', 'update')->name('admin.seo.update');
        Route::delete('/{seo}', 'delete')->name('admin.seo.delete');
    });
    //-----------------------------------------------------------------------------------------
    Route::controller(AboutController::class)->prefix('about')->group(function () {
        Route::get('', 'index')->name('admin.about.index');
        Route::get('/edit/{id}', 'edit')->name('admin.about.edit');
        Route::post('{about}/update', 'update')->name('admin.about.update');
    });
});
