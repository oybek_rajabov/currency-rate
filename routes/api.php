<?php

use App\Http\Controllers\Admin\CurrenciesController;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('import/currency/datas', [ApiController::class,'importCurrencyDatas'])->name('admin.currencies.import-currency-datas');
Route::get('update/currency/rate', [ApiController::class,'updateRate'])->name('admin.currencies.update-rate');
Route::get('banks-scraper', [ApiController::class,'scraper'])->name('admin.banks.scraper');
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
