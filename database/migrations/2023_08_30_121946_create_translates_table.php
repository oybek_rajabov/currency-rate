<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('translates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('translatable_id')->nullable()->index();
            $table->string('translatable_type')->nullable();
            $table->string('table_name')->nullable()->index('translates_table_name');
            $table->integer('field_id')->nullable()->index('translates_field_id');
            $table->string('field_name')->nullable()->index('translates_field_name');
            $table->text('field_value')->nullable()->index('translates_field_value');
            $table->string('language_url',20)->nullable()->index('translates_language_url');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('translates');
    }
};
