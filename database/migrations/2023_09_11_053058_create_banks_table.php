<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->id('id');
            $table->string('name')->nullable();
            $table->float('usd_buy')->nullable();
            $table->float('usd_sell')->nullable();
            $table->float('rub_buy')->nullable();
            $table->float('rub_sell')->nullable();
            $table->float('eur_buy')->nullable();
            $table->float('eur_sell')->nullable();
            $table->float('kzt_buy')->nullable();
            $table->float('kzt_sell')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('seo_keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('banks');
    }
};
