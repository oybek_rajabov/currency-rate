<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('languages')->delete();

        DB::table('languages')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Русский',
                    'url' => 'ru',
                    'default' => true
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => "O'zbek",
                    'url' => 'uz',
                    'default' => false
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'English',
                    'url' => 'en',
                    'default' => false
                ),
        ));
        $lastId = DB::table('languages')->orderBy('id', 'desc')->first();
        DB::statement('alter sequence languages_id_seq restart with ' . (intval($lastId->id) + 1));
    }
}
