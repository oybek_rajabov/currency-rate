<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title')</title>
    <link rel="icon" href="{{asset('assets/img/title.ico')}}"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('keywords')" name="keywords"/>
    <meta content="" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-25LL4B09VQ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-25LL4B09VQ');
    </script>

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{asset('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/style.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/style-responsive.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/theme/default.css')}}" rel="stylesheet" id="theme"/>
    <link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')}}"
          rel="stylesheet"/>
    <!-- ================== END BASE CSS STYLE ================== -->
@yield('custom_css')
<!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('assets/plugins/pace/pace.min.js')}}"></script>
    <style>
        .content {
            margin-left: 0px !important;
        }

        .page-container {
            box-shadow: none !important;
            border: 1px solid #d9e0e7;
            background-color: white;
        }
    </style>
    <!-- ================== END BASE JS ================== -->
</head>
<body class="boxed-layout">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
        <!-- begin container-fluid -->
        <div class="container-fluid">
            <!-- begin mobile sidebar expand / collapse button -->
            <div class="navbar-header">
                <a href="{{route('currencies')}}" class="navbar-brand">@lang('message.currency_rate')</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown navbar-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <span class="hidden-xs"></span> @lang('message.language')&nbsp;<b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li><a href="{{route('locale.set-lang',['locale' => 'ru'])}}">RU</a></li>
                        <li><a href="{{route('locale.set-lang',['locale' => 'uz'])}}">UZ</a></li>
                        <li><a href="{{route('locale.set-lang',['locale' => 'en'])}}">EN</a></li>
                    </ul>
                </li>
            </ul>
            <!-- end mobile sidebar expand / collapse button -->
        </div>
        <!-- end container-fluid -->
    </div>

    <!-- end #header -->

    <!-- begin #content -->

@yield('content')
<!-- end #content -->

    <!-- begin theme-panel -->
{{--    <div class="theme-panel">--}}
{{--        <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>--}}
{{--        <div class="theme-panel-content">--}}
{{--            <h5 class="m-t-0">Color Theme</h5>--}}
{{--            <ul class="theme-list clearfix">--}}
{{--                <li class="active"><a href="javascript:;" class="bg-green" data-theme="default"--}}
{{--                                      data-click="theme-selector" data-toggle="tooltip" data-trigger="hover"--}}
{{--                                      data-container="body" data-title="Default">&nbsp;</a></li>--}}
{{--                <li><a href="javascript:;" class="bg-red" data-theme="red" data-click="theme-selector"--}}
{{--                       data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">&nbsp;</a></li>--}}
{{--                <li><a href="javascript:;" class="bg-blue" data-theme="blue" data-click="theme-selector"--}}
{{--                       data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">&nbsp;</a>--}}
{{--                </li>--}}
{{--                <li><a href="javascript:;" class="bg-purple" data-theme="purple" data-click="theme-selector"--}}
{{--                       data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">&nbsp;</a>--}}
{{--                </li>--}}
{{--                <li><a href="javascript:;" class="bg-orange" data-theme="orange" data-click="theme-selector"--}}
{{--                       data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">&nbsp;</a>--}}
{{--                </li>--}}
{{--                <li><a href="javascript:;" class="bg-black" data-theme="black" data-click="theme-selector"--}}
{{--                       data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">&nbsp;</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--            <div class="divider"></div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-5 control-label double-line">Header Styling</div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <select name="header-styling" class="form-control input-sm">--}}
{{--                        <option value="1">default</option>--}}
{{--                        <option value="2">inverse</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-5 control-label">Header</div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <select name="header-fixed" class="form-control input-sm">--}}
{{--                        <option value="1">fixed</option>--}}
{{--                        <option value="2">default</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-5 control-label double-line">Sidebar Styling</div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <select name="sidebar-styling" class="form-control input-sm">--}}
{{--                        <option value="1">default</option>--}}
{{--                        <option value="2">grid</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-5 control-label">Sidebar</div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <select name="sidebar-fixed" class="form-control input-sm">--}}
{{--                        <option value="1">fixed</option>--}}
{{--                        <option value="2">default</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-5 control-label double-line">Sidebar Gradient</div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <select name="content-gradient" class="form-control input-sm">--}}
{{--                        <option value="1">disabled</option>--}}
{{--                        <option value="2">enabled</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-5 control-label double-line">Content Styling</div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <select name="content-styling" class="form-control input-sm">--}}
{{--                        <option value="1">default</option>--}}
{{--                        <option value="2">black</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row m-t-10">--}}
{{--                <div class="col-md-12">--}}
{{--                    <a href="#" class="btn btn-inverse btn-block btn-sm" data-click="reset-local-storage"><i--}}
{{--                                class="fa fa-refresh m-r-3"></i> Reset Local Storage</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
<!-- end theme-panel -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!--[if lt IE 9]>
<script src="{{asset('assets/crossbrowserjs/html5shiv.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/respond.min.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-responsive.demo.min.js')}}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/js/apps.min.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        TableManageResponsive.init();
        $("#data-table1, #data-table2").DataTable({
            responsive:true,
            paging:false,
            searching: false,
            info: false
        });


    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
@yield('custom_js')
</body>
</html>
