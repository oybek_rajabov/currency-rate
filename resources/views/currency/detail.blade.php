@extends('layouts.app')
@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)
@section('custom_css')
    <style>

        .fa-exchange {
            transition: all .3s;
            transform: scale(1);
        }

        .removable {
            transform: scale(1.1);
        }
    </style>
@endsection
@section('content')
    <div id="content" class="content">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">@lang('message.calculator')</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <!-- /.col-md-6 -->
                        <div class="row">
                            <div class="col-md-5">
                                <fieldset style="margin: 15px 10px 5px 10px;">
                                    <div class="row" style="display: flex; justify-content: center;">
                                        <div class="col-md-5 col-lg-5 col-xs-5">
                                            <div class="form-group">
                                                <select class="form-control" id="currency1" name="selectBox1"
                                                        data-parsley-required="true">
                                                    @foreach($converts as $convert)
                                                        @if($convert->base_code == $currency->base_code)
                                                            <option value="{{$convert->base_code}}"
                                                                    selected>{{$convert->base_code}}</option>
                                                        @else
                                                            <option value="{{$convert->base_code}}">{{$convert->base_code}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="number" id="currency_value1" class="form-control" value=1>
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-xs-2" style="display: flex; justify-content: center;">
                                            <div><i class="fa fa-3x fa-exchange" id="swap"></i></div>
                                        </div>
                                        <div class="col-md-5 col-lg-5 col-xs-5">
                                            <div class="form-group">
                                                <select class="form-control" id="currency2" name="selectBox2"
                                                        data-parsley-required="true">
                                                    @foreach($converts as $convert)
                                                        @if($convert->base_code == 'UZS')
                                                            <option value="{{$convert->base_code}}"
                                                                    selected>{{$convert->base_code}}</option>
                                                        @else
                                                            <option value="{{$convert->base_code}}">{{$convert->base_code}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" readonly id="currency_value2" class="form-control" value="{{$uzs_convert}}">
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </fieldset>
                            </div>
                            <!-- /.col-md-6 -->
                            <div class="col-md-7">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{$currency->base_code}}</th>
                                        <th>UZS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($calculator_table as $calculator)
                                        <tr>
                                            <td>{{$calculator['value']}}</td>
                                            <td>{{$calculator['convert']}}</td>
                                        </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">@lang('message.not found')</td>
                                            </tr>
                                            @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col-md-6 -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            <!-- /.col-md-2 -->
            <!-- /.col-md-2 -->
        <!-- /.row -->

    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#currency1', function (e) {
                e.preventDefault()
                var currency1 = $(this).val();
                var currency2 = $('#currency2').val();
                var currency_value1 = $('#currency_value1').val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })
        $(document).ready(function () {
            $(document).on('change', '#currency2', function (e) {
                e.preventDefault()
                var currency2 = $(this).val();
                var currency1 = $('#currency1').val();
                var currency_value1 = $('#currency_value1').val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })
        $(document).ready(function () {
            $(document).on('change', '#currency_value1', function (e) {
                e.preventDefault()
                var currency2 = $('#currency2').val();
                var currency1 = $('#currency1').val();
                var currency_value1 = $(this).val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })

        $(document).ready(function () {
            $(document).on('input', '#currency_value1', function (e) {
                e.preventDefault()
                var currency2 = $('#currency2').val();
                var currency1 = $('#currency1').val();
                var currency_value1 = $(this).val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })

        function convertCurrency(currency1, currency2, value) {
            // console.log(currency1, currency2, value);
            $.ajax({
                url: '{{env('HOME_LINK')}}/convert',
                type: 'post',
                data: jQuery.param({currency1: currency1, currency2: currency2, value: value}),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    $('#currency_value2').val(data);
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });
        }

        $(document).ready(function () {
            $(document).on('click', '#swap', function (e) {
                e.preventDefault()
                var temp = $('#currency1').val();
                $('#currency1').val($('#currency2').val());
                $('#currency2').val(temp);
                $(this).addClass('removable');
                setTimeout(function () {
                    $('#swap').removeClass('removable');
                }, 200);
                convertCurrency($('#currency1').val(), $('#currency2').val(), $('#currency_value1').val());
            })
        })
    </script>
@endsection
