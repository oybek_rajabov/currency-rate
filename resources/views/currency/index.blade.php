@extends('layouts.app')
@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)
@section('custom_css')
    <style>

        .fa-exchange {
            transition: all .3s;
            transform: scale(1);
        }

        .removable {
            transform: scale(1.1);
        }
    </style>
@endsection
@section('content')
    <div id="content" class="content">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">@lang('message.rates')</h4>
            </div>
            <div class="panel-body">
                {{--                /////////////////////////////////--}}
                <div class="row">
                @foreach ($datas as $data)
                    <!-- begin col-3 -->
                        @if($data['base_code'] == 'UZS')
                            @continue
                        @endif
                        <div class="col-md-3 col-sm-6">
                            <div class="widget widget-stats {{$data['background']}}">
                                <div class="stats-icon"><i class="fa fa-money"></i></div>
                                <div class="stats-info">
                                    <h4>{{$data['base_code']}}</h4>
                                    <p style="font-size: 15px !important;">1 {{$data['base_code']}}
                                        = {{$data['uzs']}}</p>
                                </div>
                                <div class="stats-link">
                                    <a href="{{route('currencies.more',['locale' => app()->getLocale(),'currency' => $data['id']])}}">@lang('message.more')
                                        <i class="fa fa-arrow-circle-o-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                    @endforeach
                </div>
                {{--                ///////////////////////////////--}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">@lang('message.calculator')</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <!-- /.col-md-6 -->
                        <fieldset style="margin: 15px 10px 5px 10px;">
                            <div class="row" style="display: flex; justify-content: center;">
                                <div class="col-md-5 col-lg-5 col-xs-5">
                                    <div class="form-group">
                                        <select class="form-control" id="currency1" name="selectBox1"
                                                data-parsley-required="true">
                                            @foreach($converts as $convert)
                                                @if($convert->base_code == 'UZS')
                                                    <option value="{{$convert->base_code}}"
                                                            selected>{{$convert->base_code}}</option>
                                                @else
                                                    <option value="{{$convert->base_code}}">{{$convert->base_code}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" id="currency_value1" class="form-control" value=1>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <div class="col-md-2 col-lg-2 col-xs-2" style="display: flex; justify-content: center;">
                                    <div><i class="fa fa-3x fa-exchange" id="swap"></i></div>
                                </div>
                                <div class="col-md-5 col-lg-5 col-xs-5">
                                    <div class="form-group">
                                        <select class="form-control" id="currency2" name="selectBox2"
                                                data-parsley-required="true">
                                            @foreach($converts as $convert)
                                                @if($convert->base_code == 'UZS')
                                                    <option value="{{$convert->base_code}}"
                                                            selected>{{$convert->base_code}}</option>
                                                @else
                                                    <option value="{{$convert->base_code}}">{{$convert->base_code}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" readonly id="currency_value2" class="form-control" value=1>
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </fieldset>
                    </div>
                </div>
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">@lang('message.banks')</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <!-- /.col-md-6 -->
                        <fieldset style="margin: 15px 10px 5px 10px;">
                            <table id="data-table1" class="table table-striped table-bordered nowrap" width="100%">
                                <thead>
                                <tr role="row">
                                    <th>#</th>
                                    <th>@lang('message.name')</th>
                                    <th>UZS</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($all_conversion_rates as $conversion_rate)
                                    <tr class="gradeA odd" role="row">
                                        <td style="width: 5%;">{{$loop->index+1}}</td>
                                        <td style="width: 10%;">{{$conversion_rate['base_code']}}</td>
                                        <td>{{$conversion_rate['uzs_value']}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6"><h5 class="text-muted m-3">@lang('message.nothing found')</h5>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <!-- /.row -->
                        </fieldset>
                    </div>
                </div>
            </div>
            <!-- /.col-md-2 -->
            <div class="col-md-8">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">@lang('message.banks')</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <!-- /.col-md-6 -->
                        <fieldset style="margin: 15px 10px 5px 10px;">
                            <table id="data-table2" class="table table-striped table-bordered">
                                <thead>
                                <tr role="row">
                                    <th>#</th>
                                    <th>@sortableLink('cur_name', trans('message.name'))</th>
                                    <th> USD @lang('message.purchase')</th>
                                    <th>USD @lang('message.sale')</th>
                                    <th>RUB @lang('message.purchase')</th>
                                    <th>RUB @lang('message.sale')</th>
                                    <th>EUR @lang('message.purchase')</th>
                                    <th>EUR @lang('message.sale')</th>
                                    <th>KZT @lang('message.purchase')</th>
                                    <th>KZT @lang('message.sale')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($banks as $bank)
                                    <tr class="gradeX odd" role="row">
                                        <td style="width: 5%;">{{$loop->index+1}}</td>
                                        <td style="width: 10%;">{{$bank->name}}</td>
                                        <td>{{$bank->usd_buy}}</td>
                                        <td>{{$bank->usd_sell}}</td>
                                        <td>{{$bank->rub_buy}}</td>
                                        <td>{{$bank->rub_sell}}</td>
                                        <td>{{$bank->eur_buy}}</td>
                                        <td>{{$bank->eur_sell}}</td>
                                        <td>{{$bank->kzt_buy}}</td>
                                        <td>{{$bank->kzt_sell}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="10"><h5 class="text-muted m-3"> @lang('message.nothing found')</h5>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table><!-- /.row -->
                        </fieldset>
                    </div>
                </div>
            </div>
            <!-- /.col-md-2 -->
        </div>

        <!-- /.row -->

        <div class="panel panel-inverse" data-sortable-id="index-1">
            <div class="panel-body">
                <div class="note note-info">
                    <p>{{footerText()}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#currency1', function (e) {
                e.preventDefault()
                var currency1 = $(this).val();
                var currency2 = $('#currency2').val();
                var currency_value1 = $('#currency_value1').val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })
        $(document).ready(function () {
            $(document).on('change', '#currency2', function (e) {
                e.preventDefault()
                var currency2 = $(this).val();
                var currency1 = $('#currency1').val();
                var currency_value1 = $('#currency_value1').val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })
        $(document).ready(function () {
            $(document).on('change', '#currency_value1', function (e) {
                e.preventDefault()
                var currency2 = $('#currency2').val();
                var currency1 = $('#currency1').val();
                var currency_value1 = $(this).val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })

        $(document).ready(function () {
            $(document).on('input', '#currency_value1', function (e) {
                e.preventDefault()
                var currency2 = $('#currency2').val();
                var currency1 = $('#currency1').val();
                var currency_value1 = $(this).val();
                convertCurrency(currency1, currency2, currency_value1)
            })
        })

        function convertCurrency(currency1, currency2, value) {
            // console.log(currency1, currency2, value);
            $.ajax({
                url: '{{env('HOME_LINK')}}/convert',
                type: 'post',
                data: jQuery.param({currency1: currency1, currency2: currency2, value: value}),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    $('#currency_value2').val(data);
                },
                error: function (data) {
                    log(data.responseText);
                }
            });
        }

        $(document).ready(function () {
            $(document).on('click', '#swap', function (e) {
                e.preventDefault()
                var temp = $('#currency1').val();
                $('#currency1').val($('#currency2').val());
                $('#currency2').val(temp);
                $(this).addClass('removable');
                setTimeout(function () {
                    $('#swap').removeClass('removable');
                }, 200);
                convertCurrency($('#currency1').val(), $('#currency2').val(), $('#currency_value1').val());
            })
        })
    </script>
@endsection
