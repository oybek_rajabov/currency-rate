@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.seo.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> SEO</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Редактирование</h4>
            </div>
            <div class="panel-body">
                <form action="{{route('admin.seo.update',['seo' => $seo->id])}}" method="POST"
                      class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="type">Тип</label>
                                <select class="form-control" name="type">
                                    @foreach(seoTypes() as $key => $val)
                                        <option value="{{$key}}" @if($seo->type == $key) selected @endif>{{$val}}</option>
                                    @endforeach
                                </select>
                                @error('type')
                                <div class="alert alert-danger fade in m-b-15">
                                    {{$message}}
                                    <span class="close" data-dismiss="alert">×</span>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#ru" data-toggle="tab" aria-expanded="true">RU</a></li>
                        <li class=""><a href="#uz" data-toggle="tab" aria-expanded="false">UZ</a></li>
                        <li class=""><a href="#en" data-toggle="tab" aria-expanded="false">EN</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ru">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="title_ru">SEO title</label>
                                        <input type="text" class="form-control" name="title_ru" id="title_ru" value="{{$translates['title']['ru'] ?? ''}}">
                                        @error('title_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="description_ru">SEO description</label>
                                        <textarea class="form-control" name="description_ru" id="description_ru" >{{$translates['description']['ru'] ?? ''}}</textarea>
                                        @error('description_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="keywords_ru">SEO keywords</label>
                                        <textarea class="form-control" name="keywords_ru" id="keywords_ru">{{$translates['keywords']['ru'] ?? ''}}</textarea>
                                        @error('keywords_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="tab-pane" id="uz">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="title_uz">SEO title</label>
                                        <input type="text" class="form-control" name="title_uz" id="title_uz" value="{{$translates['title']['uz'] ?? ''}}">
                                        @error('title_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="description_uz">SEO description</label>
                                        <textarea class="form-control" name="description_uz" id="description_uz">{{$translates['description']['uz'] ?? ''}}</textarea>
                                        @error('description_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="keywords_uz">SEO keywords</label>
                                        <textarea class="form-control" name="keywords_uz" id="keywords_uz">{{$translates['keywords']['uz'] ?? ''}}</textarea>
                                        @error('keywords_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="en">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="title_en">SEO title</label>
                                        <input type="text" class="form-control" name="title_en" id="title_en" value="{{$translates['title']['en'] ?? ''}}">
                                        @error('title_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="description_en">SEO description</label>
                                        <textarea class="form-control" name="description_en" id="description_en">{{$translates['description']['en'] ?? ''}}</textarea>
                                        @error('description_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="keywords_en">SEO keywords</label>
                                        <textarea class="form-control" name="keywords_en" id="keywords_en">{{$translates['keywords']['en'] ?? ''}}</textarea>
                                        @error('keywords_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <a href="{{redirect()->getUrlGenerator()->previous()}}"
                       class="btn btn-sm btn-default m-r-5">Назад</a>
                    <button type="submit" class="btn btn-sm btn-success m-r-5">Обновить</button>
                </form>
            </div>

        </div>
    </div>
@endsection
