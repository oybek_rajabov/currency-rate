@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="active"><a href="#">Главная</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> SEO</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{route('admin.seo.create')}}" type="button" class="btn btn-success btn-xs"><i
                                class="fa fa-plus-circle"></i> Создать</a>
                </div>
                <h4 class="panel-title">#</h4>
            </div>
            <div class="panel-body">
                <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="data-table"
                                   class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid"
                                   aria-describedby="data-table_info">
                                <thead>
                                <tr role="row">
                                    <th>#</th>
                                    <th>Тип</th>
                                    <th>Мета-заголовок</th>
                                    <th>Мета-описание</th>
                                    <th>Мета-ключевые слова</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($seos as $seo)
                                    <tr class="gradeA odd" role="row">
                                        <td style="width: 5%;">{{$loop->index+1}}</td>
                                        <td style="width: 10%;">@lang('message.'.$seo->type)</td>
                                        <td>{{$seo->title}}</td>
                                        <td>{{$seo->description}}</td>
                                        <td>{{$seo->keywords}}</td>
                                        <td>
                                            <div style="display: flex; justify-content: center;">
                                                <a href="{{route('admin.seo.show', $seo->id)}}" class="btn btn-success btn-icon btn-circle btn-lg" style="margin-right: 3px"><i class="fa fa-eye"></i></a>
                                                <a href="{{route('admin.seo.edit',$seo->id)}}" class="btn btn-warning btn-icon btn-circle btn-lg"style="margin-right: 3px" ><i class="fa fa-pencil"></i></a>
                                                <a href="#modal-dialog{{$seo->id}}" data-toggle="modal" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
                                            </div>
                                            <div class="modal fade" id="modal-dialog{{$seo->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">Уведомление</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Вы хотите удалить?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                                                            <a href="#" onclick="deleteSeo({{$seo->id}})" class="btn btn-sm btn-success">Да</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr><td colspan="6"><h5 class="text-muted m-3"> Ничего не найдено</h5></td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('custom_js')
    <script>
        function deleteSeo(id){
            console.log(21);
            $.ajax({
                url: '{{env('HOME_LINK')}}/admin/admin/seo/'+ id,
                type: 'delete',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    $('#modal-dialog'+id).modal().hide();
                    if(data){
                        $.gritter.add({
                            title: "Успешно!",
                            text: "Seo успешно удалена!",
                            time: 2,
                            after_close: function (e) {
                                location.reload(true);
                            }
                        });
                    }else {
                        $.gritter.add({
                            title: "Ошибка!",
                            text: "Произошла ошибка при удалении seo!",
                            time: 2,
                            after_close: function (e) {
                                location.reload(true);
                            }
                        });
                    }

                },
                error: function (data) {
                    $('#modal-dialog'+id).modal().hide();
                    alert(data.responseText);
                    setTimeout(function(){
                        location.reload(true);
                    }, 3000);
                }
            });
        }
    </script>
@endsection
