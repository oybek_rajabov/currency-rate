@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.seo.index')}}">Главная</a></li>
            <li class="active"><a href="#">Создать</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> SEO</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Создать</h4>
            </div>
            <div class="panel-body">
                <form action="{{route('admin.seo.insert')}}" method="POST"
                      class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="type">Тип</label>
                                <select class="form-control" name="type">
                                    <option value="main_page" selected>Главная страница</option>
                                </select>
                                @error('type')
                                <div class="alert alert-danger fade in m-b-15">
                                    {{$message}}
                                    <span class="close" data-dismiss="alert">×</span>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#ru" data-toggle="tab" aria-expanded="true">RU</a></li>
                        <li class=""><a href="#uz" data-toggle="tab" aria-expanded="false">UZ</a></li>
                        <li class=""><a href="#en" data-toggle="tab" aria-expanded="false">EN</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ru">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="seo_title_ru">SEO title</label>
                                        <input type="text" class="form-control" name="seo_title_ru" id="seo_title_ru">
                                        @error('seo_title_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">SEO description</label>
                                        <textarea class="form-control" name="seo_description_ru"></textarea>
                                        @error('seo_description_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">SEO keywords</label>
                                        <textarea class="form-control" name="seo_keywords_ru"></textarea>
                                        @error('seo_keywords_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="tab-pane" id="uz">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="cur_name_uz">SEO title</label>
                                        <input type="text" class="form-control" name="seo_title_uz" id="seo_title_uz">
                                        @error('seo_title_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_uz">SEO description</label>
                                        <textarea class="form-control" name="seo_description_uz"></textarea>
                                        @error('seo_description_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_uz">SEO keywords</label>
                                        <textarea class="form-control" name="seo_keywords_uz"></textarea>
                                        @error('seo_keywords_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="en">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="cur_name_en">SEO title</label>
                                        <input type="text" class="form-control" name="seo_title_en" id="seo_title_en">
                                        @error('seo_title_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_en">SEO description</label>
                                        <textarea class="form-control" name="seo_description_en"></textarea>
                                        @error('seo_description_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_en">SEO keywords</label>
                                        <textarea class="form-control" name="seo_keywords_en"></textarea>
                                        @error('seo_keywords_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- /.row -->
                    <a href="{{redirect()->getUrlGenerator()->previous()}}"
                       class="btn btn-sm btn-default m-r-5">Назад</a>
                    <button type="submit" class="btn btn-sm btn-success m-r-5">Создать</button>
                </form>
            </div>

        </div>
    </div>
@endsection
