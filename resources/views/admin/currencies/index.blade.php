@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="active"><a href="#">Главная</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> Валюты</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{route('admin.currencies.update-rate')}}" type="button" class="btn btn-success btn-xs"><i
                                class="fa fa-refresh"></i> Обновить курс валюты</a>
                    <a href="{{route('admin.currencies.import-currency-datas')}}" type="button" class="btn btn-inverse btn-xs"><i
                                class="fa fa-download"></i>Импортировать валютыy</a>
                </div>
                <h4 class="panel-title">#</h4>
            </div>
            <div class="panel-body">
                <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="data-table"
                                   class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid"
                                   aria-describedby="data-table_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="data-table" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending"
                                        style="width: 198.2px;">#
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="data-table" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending"
                                        style="width: 198.2px;">@sortableLink('base_code', 'Валюта')
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1"
                                        aria-label="Browser: activate to sort column ascending" style="width: 260.2px;">
                                        @sortableLink('cur_name', 'Название')
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1"
                                        aria-label="Platform(s): activate to sort column ascending"
                                        style="width: 235.2px;">Обновлено
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1"
                                        aria-label="Engine version: activate to sort column ascending"
                                        style="width: 170.2px;">Главная страница
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1"
                                        aria-label="Engine version: activate to sort column ascending"
                                        style="width: 170.2px;">Действие
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($currencies as $currency)
                                    <tr class="gradeA odd" role="row">
                                        <td style="width: 5%;">{{($currencies->currentpage()-1)*$currencies->perpage()+$loop->index+1}}</td>
                                        <td style="width: 10%;">{{$currency->base_code}}</td>
                                        <td>{{$currency->cur_name}}</td>
                                        <td>{{$currency->rate_update_at}}</td>
                                        <td style="width: 8%;">
                                            <input type="checkbox" data-render="switchery" id="myswitcher" data-currency-id="{{$currency->id}}" data-theme="default" @if($currency->is_main) checked @else unchecked @endif />
                                        <td>
                                            <div style="display: flex; justify-content: center;">
                                                <a href="{{route('admin.currencies.show', $currency->id)}}" class="btn btn-success btn-icon btn-circle btn-lg" style="margin-right: 3px"><i class="fa fa-eye"></i></a>
                                                <a href="{{route('admin.currencies.edit',$currency->id)}}" class="btn btn-warning btn-icon btn-circle btn-lg"style="margin-right: 3px" ><i class="fa fa-pencil"></i></a>
                                                <a href="#modal-dialog{{$currency->id}}" data-toggle="modal" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
                                            </div>
                                            <div class="modal fade" id="modal-dialog{{$currency->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">Уведомление</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Вы хотите удалить?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                                                            <a href="#" onclick="deleteCurrency({{$currency->id}})" class="btn btn-sm btn-success">Да</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr><td colspan="6"><h5 class="text-muted m-3"> Ничего не найдено</h5></td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="display: flex; justify-content: center;">
                            <div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate">
                                {{$currencies->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#myswitcher', function (e) {
                e.preventDefault()
                data_currency_id = $(this).attr('data-currency-id');
                $.ajax({
                    url: '{{env('HOME_LINK')}}/admin/admin/currencies/change-main-currency/'+ data_currency_id,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });
            })
        });

        function deleteCurrency(id){
            $.ajax({
                url: '{{env('HOME_LINK')}}/admin/admin/currencies/'+ id,
                type: 'delete',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    $('#modal-dialog'+id).modal().hide();
                    if(data){
                        $.gritter.add({
                            title: "Успешно!",
                            text: "Валюта успешно удалена!",
                            time: 2,
                            after_close: function (e) {
                                location.reload(true);
                            }
                        });
                    }else {
                        $.gritter.add({
                            title: "Ошибка!",
                            text: "Произошла ошибка при удалении валюты!",
                            time: 2,
                            after_close: function (e) {
                                location.reload(true);
                            }
                        });
                    }

                },
                error: function (data) {
                    $('#modal-dialog'+id).modal().hide();
                    alert(data.responseText);
                    setTimeout(function(){
                        location.reload(true);
                    }, 3000);
                }
            });
        }
    </script>
@endsection
