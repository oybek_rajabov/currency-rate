@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.currencies.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> Валюты</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Редактирование</h4>
            </div>
            <div class="panel-body">
                <form action="{{route('admin.currencies.update',['currency' => $currency->id])}}" method="POST"
                      class="form-horizontal">
                    @method('POST')
                    @csrf
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#ru" data-toggle="tab" aria-expanded="true">RU</a></li>
                        <li class=""><a href="#uz" data-toggle="tab" aria-expanded="false">UZ</a></li>
                        <li class=""><a href="#en" data-toggle="tab" aria-expanded="false">EN</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ru">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">Название</label>
                                        <input type="text" class="form-control" name="cur_name_ru" id="cur_name_ru"
                                               value="{{$translates['cur_name']['ru'] ?? ''}}">
                                        @error('cur_name_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">SEO title</label>
                                        <input type="text" class="form-control" name="seo_title_ru" id="seo_title_ru"
                                               value="{{$translates['seo_title']['ru'] ?? ''}}">
                                        @error('seo_title_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">SEO description</label>
                                        <textarea class="form-control" name="seo_description_ru">{{$translates['seo_description']['ru'] ?? ''}}</textarea>
                                        @error('seo_description_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">SEO keywords</label>
                                    <textarea class="form-control" name="seo_keywords_ru">{{$translates['seo_keywords']['ru'] ?? ''}}</textarea>
                                        @error('seo_keywords_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="tab-pane" id="uz">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_ru">Nomi</label>
                                        <input type="text" class="form-control" name="cur_name_uz" id="cur_name_uz"
                                               value="{{$translates['cur_name']['uz'] ?? ''}}">
                                        @error('cur_name_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_uz">SEO title</label>
                                        <input type="text" class="form-control" name="seo_title_uz" id="seo_title_uz"
                                               value="{{$translates['seo_title']['uz'] ?? ''}}">
                                        @error('seo_title_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_uz">SEO description</label>
                                        <textarea class="form-control" name="seo_description_uz">{{$translates['seo_description']['uz'] ?? ''}}</textarea>
                                        @error('seo_description_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_uz">SEO keywords</label>
                                        <textarea class="form-control" name="seo_keywords_uz">{{$translates['seo_keywords']['uz'] ?? ''}}</textarea>
                                        @error('seo_keywords_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="en">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_en">Name</label>
                                        <input type="text" class="form-control" name="cur_name_en" id="cur_name_en"
                                               value="{{$translates['cur_name']['en'] ?? ''}}">
                                        @error('cur_name_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_en">SEO title</label>
                                        <input type="text" class="form-control" name="seo_title_en" id="seo_title_en"
                                               value="{{$translates['seo_title']['en']  ?? ''}}">
                                        @error('seo_title_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="cur_name_en">SEO description</label>
                                        <textarea class="form-control" name="seo_description_en">{{$translates['seo_description']['en'] ?? ''}}</textarea>
                                        @error('seo_description_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cur_name_en">SEO keywords</label>
                                        <textarea class="form-control" name="seo_keywords_en">{{$translates['seo_keywords']['en'] ?? ''}}</textarea>
                                        @error('seo_keywords_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Код</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="base_code"
                                               value="{{$currency->base_code}}">
                                        @error('base_code')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Страна</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="country"
                                               value="{{$currency->country}}">
                                        @error('country')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <a href="{{redirect()->getUrlGenerator()->previous()}}"
                       class="btn btn-sm btn-default m-r-5">Назад</a>
                    <button type="submit" class="btn btn-sm btn-success m-r-5">Обновить</button>
                </form>
            </div>

        </div>
    </div>
@endsection
