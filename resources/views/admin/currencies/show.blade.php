@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.currencies.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> Валюты</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">#</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Валюта</th>
                        <th>Название</th>
                        <th>Обновлено</th>
                        <th>Главная страница</th>
                        <th>Курс валюты</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$currency->id}}</td>
                        <td><span class="label label-info">{{$currency->base_code}}</span></td>
                        <td><span class="badge badge-info">{{$currency->cur_name}}</span></td>
                        <td><span class="badge badge-info badge-square">{{$currency->rate_update_at}}</span></td>
                        <td><span class="badge badge-info badge-square">{{$currency->is_main}}</span></td>
                    </tr>
                    <tr>
                        <td colspan="6" >
                            <div style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                                @forelse($rates as $key => $val)
                                   <div><span class="badge badge-success badge-square">{{$key}}</span>:{{$val}} </div>
                                @empty
                                @endforelse
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p>
                    <a href="{{route('admin.currencies.edit',$currency->id)}}" class="btn btn-sm btn-warning m-r-5"><i class="fa fa-pencil"></i>Редактировать</a>
                    <a href="javascript:;" class="btn btn-sm btn-default"><i class="fa fa-reply"></i>Отмена</a>
                </p>
            </div>
        </div>
    </div>
@endsection
