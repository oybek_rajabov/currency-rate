@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.banks.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> Валюты</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Редактирование</h4>
            </div>
            <div class="panel-body">
                <form action="{{route('admin.banks.update',['bank' => $bank->id])}}" method="POST"
                      class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Название</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name"
                                           value="{{$bank->name}}">
                                    @error('base_code')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">USD Покупка</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="usd_buy"
                                           value="{{$bank->usd_buy}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">USD Продажа</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="usd_sell"
                                           value="{{$bank->usd_sell}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">RUB Покупка</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="rub_buy"
                                           value="{{$bank->rub_buy}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">RUB Продажа</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="rub_sell"
                                           value="{{$bank->rub_sell}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-4 -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">EUR Покупка</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="eur_buy"
                                           value="{{$bank->eur_buy}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">EUR Продажа</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="eur_sell"
                                           value="{{$bank->eur_sell}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">KZT Покупка</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="kzt_buy"
                                           value="{{$bank->kzt_buy}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">KZT Продажа</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="kzt_sell"
                                           value="{{$bank->kzt_sell}}">
                                    @error('country')
                                    <div class="alert alert-danger fade in m-b-15">
                                        {{$message}}
                                        <span class="close" data-dismiss="alert">×</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <a href="{{redirect()->getUrlGenerator()->previous()}}"
                       class="btn btn-sm btn-default m-r-5">Назад</a>
                    <button type="submit" class="btn btn-sm btn-success m-r-5">Обновить</button>
                </form>
            </div>

        </div>
    </div>
@endsection
