@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.banks.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> Банки</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">#{{$bank->id}}</h4>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><h4>Название</h4></label>
                                <div class="col-md-8">
                                    {{$bank->name}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><h4>USD</h4></label>
                                <div class="col-md-8">
                                    <p><b>Покупка:</b> &nbsp;{{$bank->usd_buy}}</p>
                                    <p><b>Продажа:</b> &nbsp;{{$bank->usd_sell}}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><h4>RUB</h4></label>
                                <div class="col-md-8">
                                    <p><b>Покупка:</b> &nbsp;{{$bank->rub_buy}}</p>
                                    <p><b>Продажа:</b> &nbsp;{{$bank->rub_sell}}</p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><h4>EUR</h4></label>
                                <div class="col-md-8">
                                    <p><b>Покупка:</b> &nbsp;{{$bank->eur_buy}}</p>
                                    <p><b>Продажа:</b> &nbsp;{{$bank->eur_sell}}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><h4>KZT</h4></label>
                                <div class="col-md-8">
                                    <p><b>Покупка:</b> &nbsp;{{$bank->kzt_buy}}</p>
                                    <p><b>Продажа:</b> &nbsp;{{$bank->kzt_sell}}</p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <!-- /.col-md-6 -->

                </div>
                <p>
                    <a href="{{route('admin.banks.edit',$bank->id)}}" class="btn btn-sm btn-warning m-r-5"><i class="fa fa-pencil"></i>Редактировать</a>
                    <a href="{{route('admin.banks.index')}}" class="btn btn-sm btn-default"><i class="fa fa-reply"></i>Отмена</a>
                </p>
            </div>
        </div>
    </div>
@endsection
