@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="active"><a href="#">Главная</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> О сайте</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <h1></h1>
                </div>
                <h4 class="panel-title">#</h4>
            </div>
            <div class="panel-body">
                <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="data-table"
                                   class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid"
                                   aria-describedby="data-table_info">
                                <thead>
                                <tr role="row">
                                    <th>Текст нижнего колонтитула</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr class="gradeA odd" role="row">
                                        <td style="width: 5%;">
                                            <p><b>RU:</b> &nbsp;{{$translates['footer_text']['ru']}}</p>
                                            <p><b>UZ:</b> &nbsp;{{$translates['footer_text']['uz']}}</p>
                                            <p><b>EN:</b>  {{$translates['footer_text']['en']}}</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="{{route('admin.about.edit',$about->id)}}" class="btn btn-warning"style="margin-right: 3px" >Редактировать</a>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('custom_js')
@endsection
