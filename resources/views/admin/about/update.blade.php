@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.about.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> SEO</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Редактирование</h4>
            </div>
            <div class="panel-body">
                <form action="{{route('admin.about.update',['about' => $about->id])}}" method="POST"
                      class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                @error('type')
                                <div class="alert alert-danger fade in m-b-15">
                                    {{$message}}
                                    <span class="close" data-dismiss="alert">×</span>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#ru" data-toggle="tab" aria-expanded="true">RU</a></li>
                        <li class=""><a href="#uz" data-toggle="tab" aria-expanded="false">UZ</a></li>
                        <li class=""><a href="#en" data-toggle="tab" aria-expanded="false">EN</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ru">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="footer_text_ru">Footer</label>
                                        <input type="text" class="form-control" name="footer_text_ru" id="footer_text_ru" value="{{$translates['footer_text']['ru'] ?? ''}}">
                                        @error('footer_text_ru')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="uz">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="footer_text_uz">Footer</label>
                                        <input type="text" class="form-control" name="footer_text_uz" id="footer_text_uz" value="{{$translates['footer_text']['uz'] ?? ''}}">
                                        @error('footer_text_uz')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="en">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="footer_text_en">Footer</label>
                                        <input type="text" class="form-control" name="footer_text_en" id="footer_text_en" value="{{$translates['footer_text']['en'] ?? ''}}">
                                        @error('footer_text_en')
                                        <div class="alert alert-danger fade in m-b-15">
                                            {{$message}}
                                            <span class="close" data-dismiss="alert">×</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <a href="{{redirect()->getUrlGenerator()->previous()}}"
                       class="btn btn-sm btn-default m-r-5">Назад</a>
                    <button type="submit" class="btn btn-sm btn-success m-r-5">Обновить</button>
                </form>
            </div>

        </div>
    </div>
@endsection
