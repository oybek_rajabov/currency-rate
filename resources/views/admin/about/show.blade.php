@extends('admin.layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="{{route('admin.about.index')}}">Главная</a></li>
            <li class="active"><a href="#">Просмотр</a></li>
        </ol>                                                                                                        
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> Seo</h1>
        <!-- end page-header -->
        <div class="panel panel-inverse" data-sortable-id="ui-general-3">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">#{{$about->id}}</h4>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><h4>Текст нижнего колонтитула</h4></label>
                                <div class="col-md-8">
                                    <p><b>RU:</b> &nbsp;{{$translate['footer_text']['ru']}}</p>
                                    <p><b>UZ:</b> &nbsp;{{$translate['footer_text']['uz']}}</p>
                                    <p><b>EN:</b>  {{$translate['footer_text']['en']}}</p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <p>
                    <a href="{{route('admin.about.edit',$about->id)}}" class="btn btn-sm btn-warning m-r-5"><i class="fa fa-pencil"></i>Редактировать</a>
                    <a href="{{route('admin.about.index')}}" class="btn btn-sm btn-default"><i class="fa fa-reply"></i>Отмена</a>
                </p>
            </div>
        </div>
    </div>
@endsection
